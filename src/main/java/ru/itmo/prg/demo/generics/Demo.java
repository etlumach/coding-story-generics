package ru.itmo.prg.demo.generics;

public class Demo {
    public static void main (String[] args){
        int max = max(new Integer[] {1, 3, 5, 7, 9});
        System.out.println(max);

    }

    static  <T extends Comparable<T>> T max (T[] array){

        if (array == null || array.length == 0){
            throw new IllegalArgumentException();
        }

        T max = array[0];
        for (T element : array){
            if (element.compareTo(max) > 0){
                max = element;
            }
        }

        return max;

    }
}
